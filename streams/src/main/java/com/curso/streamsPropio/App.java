package com.curso.streamsPropio;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class App {

	public static void main(String[] args) {
		
		Coach c1 = new Coach(1, "Pepe Pérez", 10);
		Coach c2 = new Coach(2, "Jose Jimenez", 3);
		Coach c3 = new Coach(3, "Antonio Alvarez", 0);
		
		List<TotalPlayerInfractions> infractions = new ArrayList<TotalPlayerInfractions>();
		TotalPlayerInfractions tpi1 = new TotalPlayerInfractions(1, "Cipriano Romualdo", c1, 5000.00);
		TotalPlayerInfractions tpi2 = new TotalPlayerInfractions(2, "Leo Mesias", c2, 2000.00);
		TotalPlayerInfractions tpi3 = new TotalPlayerInfractions(3, "Djemba-Djemba", c3, 100.00);
		
		infractions.add(tpi1);
		infractions.add(tpi2);
		infractions.add(tpi3);
		
		
		infractions.add(new TotalPlayerInfractions(4, "Antonio Recio", c1, 15.00));
		infractions.add(new TotalPlayerInfractions(5, "Homer Simpson", c2, 4500.00));
		infractions.add(new TotalPlayerInfractions(6, "Peter Griffin", c3, 1000.00));
		
		App.iterateWithStream(infractions);
		System.out.println("**************************************************************");
		App.iterateStreamIterator(infractions);
		
		
		
	}
	
	//Recorriendo con Stream
	private static void iterateWithStream(List<TotalPlayerInfractions> infraction) {
		Stream<TotalPlayerInfractions> infractionStream = infraction.stream();
		infractionStream.forEach(singleAmount -> System.out.println(singleAmount));
	}
	
	//Recorriendo con Stream + Iterator (Las multas mayores a 1500 euros)
	private static void iterateStreamIterator(List<TotalPlayerInfractions> infraction) {
		Stream<TotalPlayerInfractions> infractionStream = infraction.stream();
		Iterator<TotalPlayerInfractions> it = infractionStream.filter(singleAmount -> {System.out.println("--> " + singleAmount);
		return singleAmount.getTotalAmount() > 1500.00;
		}).iterator();
		System.out.println("Infractions bigger than 1500");
		System.out.println("-----------------------------------");
		
		while(it.hasNext()) {
			System.out.println("Bigger than 1500!!!!!");
			System.out.println(it.next());
			System.out.println("**************************");
		}
	}

}
