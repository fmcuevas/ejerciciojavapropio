package com.curso.streamsPropio;

public class PlayerAmount {
	
	private Integer id;
	private Double totalperSeason;
	
	public PlayerAmount(Integer id, Double totalperSeason) {
		this.id = id;
		this.totalperSeason = totalperSeason;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getTotalperSeason() {
		return totalperSeason;
	}

	public void setTotalperSeason(Double totalperSeason) {
		this.totalperSeason = totalperSeason;
	}

	@Override
	public String toString() {
		return "PlayerAmount [id=" + id + ", totalperSeason=" + totalperSeason + "]";
	}
	
	
	
	

}
