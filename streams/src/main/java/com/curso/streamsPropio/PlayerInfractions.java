package com.curso.streamsPropio;

public class PlayerInfractions {
	
	private Integer id;
	private Double infractionCode;
	private Double infractionPrice;
	private String infractionDescription;
	
	public PlayerInfractions(Integer id, Double infractionCode, Double infractionPrice, String infractionDescription) {
		super();
		this.id = id;
		this.infractionCode = infractionCode;
		this.infractionPrice = infractionPrice;
		this.infractionDescription = infractionDescription;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getInfractionCode() {
		return infractionCode;
	}

	public void setInfractionCode(Double infractionCode) {
		this.infractionCode = infractionCode;
	}

	public Double getInfractionPrice() {
		return infractionPrice;
	}

	public void setInfractionPrice(Double infractionPrice) {
		this.infractionPrice = infractionPrice;
	}

	public String getInfractionDescription() {
		return infractionDescription;
	}

	public void setInfractionDescription(String infractionDescription) {
		this.infractionDescription = infractionDescription;
	}
	
	
	
	

}
