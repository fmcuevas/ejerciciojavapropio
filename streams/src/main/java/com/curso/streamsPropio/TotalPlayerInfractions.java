package com.curso.streamsPropio;

import java.util.List;

public class TotalPlayerInfractions {
	
	private Integer id;
	private String player;
	private Coach coach;
	private Double totalAmount;
	private List<PlayerInfractions> infractions;
	
	public TotalPlayerInfractions(){
		
	}

	public TotalPlayerInfractions(Integer id, String player, Coach coach, Double totalAmount) {
		this.id = id;
		this.player = player;
		this.coach = coach;
		this.totalAmount = totalAmount;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<PlayerInfractions> getInfractions() {
		return infractions;
	}

	public void setInfractions(List<PlayerInfractions> infractions) {
		this.infractions = infractions;
	}

	@Override
	public String toString() {
		return "TotalPlayerInfractions [id=" + id + ", player=" + player + ", coach=" + coach + ", totalAmount="
				+ totalAmount + ", infractions=" + infractions + "]";
	}
	
	public int compareTo(TotalPlayerInfractions tpi) {
		return totalAmount.intValue()-tpi.totalAmount.intValue();
	}
	
	

}
