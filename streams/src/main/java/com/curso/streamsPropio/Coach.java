package com.curso.streamsPropio;

public class Coach {
	
	private int id;
	private String name;
	private int titles;
	
	public Coach() {
		
	}

	public Coach(int id, String name, int titles) {
		super();
		this.id = id;
		this.name = name;
		this.titles = titles;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTitles() {
		return titles;
	}

	public void setTitles(int titles) {
		this.titles = titles;
	}

	@Override
	public String toString() {
		return "Coach [id=" + id + ", name=" + name + ", titles=" + titles + "]";
	}
	
	

}
