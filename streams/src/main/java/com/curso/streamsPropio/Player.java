package com.curso.streamsPropio;

public class Player {
	
	private Integer id;
	private String name;
	private Double age;
	private Double weight;
	
	public Player() {
		
		
	}
	
	public Player(Integer id, String name, Double age, Double weight) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.weight = weight;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAge() {
		return age;
	}

	public void setAge(Double age) {
		this.age = age;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", age=" + age + ", weight=" + weight + "]";
	}
	
	
	
	

}
