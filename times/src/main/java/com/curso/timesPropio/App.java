package com.curso.timesPropio;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class App {
	
	public static void main(String[] args) {
		
		App.zoneDateTime();
		System.out.println("********************************************");
		App.javaDateFormatter();
		
	}
	
	public static void zoneDateTime() {
		ZonedDateTime zdt = ZonedDateTime.now();
		System.out.println(zdt);
		
		ZoneId southAfrica = ZoneId.of("Africa/Johannesburg");
		ZoneId cambodia = ZoneId.of("Asia/Phnom_Penh");
		
		ZonedDateTime zoneSa = ZonedDateTime.of(LocalDateTime.now(), southAfrica);
		ZonedDateTime zoneCmb = ZonedDateTime.of(LocalDateTime.now(), cambodia);
		
		System.out.println("South Africa: " + zoneSa.toLocalDateTime());
		System.out.println("Cambodia: " + zoneCmb.toLocalDateTime());
	}
	
	public static void javaDateFormatter() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate ld = LocalDate.parse("22/05/1992", dtf);
		System.out.println(ld);
	}

}
