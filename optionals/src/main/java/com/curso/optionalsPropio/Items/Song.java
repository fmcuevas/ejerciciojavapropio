package com.curso.optionalsPropio.Items;

import java.util.Optional;

public class Song {
	
	private Integer id;
	private String name;
	private Optional<Cd> cdName;
	
	public Song(Integer id, String name, Optional<Cd> cdName) {
		super();
		this.id = id;
		this.name = name;
		this.cdName = cdName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<Cd> getCdName() {
		return cdName;
	}

	public void setCdName(Optional<Cd> cdName) {
		this.cdName = cdName;
	}

	@Override
	public String toString() {
		return "Song [id=" + id + ", name=" + name + ", cdName=" + cdName + "]";
	}
	

}
