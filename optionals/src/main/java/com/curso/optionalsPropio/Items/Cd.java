package com.curso.optionalsPropio.Items;

import java.util.Optional;

public class Cd {
	
	private Optional<String> name;
	private Optional<Artist> artist;
	
	public Cd() {
		name = Optional.empty();
		artist = Optional.empty();
	}
	
	
	public Cd(String name, Optional<Artist> artist) {
		this.name = Optional.of(name);
		this.artist = artist;
	}


	public Optional<String> getName() {
		return name;
	}


	public void setName(Optional<String> name) {
		this.name = name;
	}


	public Optional<Artist> getArtist() {
		return artist;
	}


	public void setArtist(Optional<Artist> artist) {
		this.artist = artist;
	}


	@Override
	public String toString() {
		return "Cd [name=" + name + ", artist=" + artist + "]";
	}
	

}
