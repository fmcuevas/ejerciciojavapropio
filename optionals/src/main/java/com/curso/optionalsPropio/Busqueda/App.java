package com.curso.optionalsPropio.Busqueda;

import java.util.Optional;

import com.curso.optionalsPropio.Items.Artist;
import com.curso.optionalsPropio.Items.Cd;
import com.curso.optionalsPropio.Items.Song;

public class App {

	public static void main(String[] args) {
		
		System.out.println("-------------------- Busqueda de Canciones ----------------");
		App.BusquedaAnidadaCanciones(1);
		System.out.println("-----------------------------------------------------------");
		App.BusquedaAnidadaCanciones(2);
		System.out.println("-----------------------------------------------------------");
		App.BusquedaAnidadaCanciones(3);
		System.out.println("-----------------------------------------------------------");
		App.BusquedaAnidadaCanciones(4);
		System.out.println("-----------------------------------------------------------");
		
		System.out.println("--------------------- Solo interesa el t�tulo -------------");
		App.BusquedaLambdaCanciones(1);
		System.out.println("-----------------------------------------------------------");
		App.BusquedaLambdaCanciones(2);
		System.out.println("-----------------------------------------------------------");
		App.BusquedaLambdaCanciones(3);
		System.out.println("-----------------------------------------------------------");
		App.BusquedaLambdaCanciones(4);
		System.out.println("-----------------------------------------------------------");
		System.out.println("----------------------------- FIN -------------------------");

	}
	
	//B�squeda Anidada
	private static void BusquedaAnidadaCanciones(Integer id) {
		ResultSearch rs = new ResultSearch();
		
		Optional<Song> firstAnid = rs.searchOptional(id);
		if(firstAnid.isPresent()) {
			Optional<Cd> secondAnid = firstAnid.get().getCdName();
			if(secondAnid.isPresent()) {
				Optional<Artist> thirdAnid = secondAnid.get().getArtist();
				if(thirdAnid.isPresent()) {
					System.out.println("Artista --> " + firstAnid);
				}else {
					System.out.println("No hay artista...");
				}
			}else {
				System.out.println("CD --> " + firstAnid);
			}
		}
		
	}
	
	//B�squeda mediante lambda
	private static void BusquedaLambdaCanciones(Integer id) {
		ResultSearch rs = new ResultSearch();
		
		Optional<Song> lmbdsong = rs.searchOptional(id);
		lmbdsong.ifPresent(lmbd -> System.out.println(lmbd.getName()));
	}

}
