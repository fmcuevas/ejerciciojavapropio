package com.curso.optionalsPropio.Busqueda;

import java.util.Optional;

import com.curso.optionalsPropio.Items.Artist;
import com.curso.optionalsPropio.Items.Cd;
import com.curso.optionalsPropio.Items.Song;

public class ResultSearch {
	
	public Optional<Song> searchOptional(Integer id){
		
		switch(id) {
		case 1: return Optional.of(new Song (1, "Te duele", Optional.of(new Cd("Single", Optional.of(new Artist("MC", "Manuel Cort�s"))))));
		case 2: return Optional.of(new Song (2, "Juanito Alima�a", Optional.of(new Cd("Indestructible", Optional.of(new Artist("DeC", "Diego El Cigala"))))));
		case 3: return Optional.of(new Song (3, "S.O.S", Optional.of(new Cd("Amar Duele", Optional.of(new Artist("FLT", "Falete"))))));
		case 4: return Optional.of(new Song (4, "!Hola, mi amor!", Optional.empty()));
		default: return Optional.empty();
		}
	}

}
