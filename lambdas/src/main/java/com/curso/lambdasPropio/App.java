package com.curso.lambdasPropio;

public class App {

	public static void main(String[] args) {
		
		
		App.usingInterfaceLambdaWithOneParameter();
		System.out.println("------------------------------------------------------------------");
		App.usingInterfaceLambdaWithTwoParameters();
		System.out.println("---------------------------------- FIN ---------------------------");

	}
	
	//Usar lambdas con un parámetro para nombrar una ciudad
	public static void usingInterfaceLambdaWithOneParameter() {
				
		String dondeVivo = "Marbella";
		IinterfaceOneParameter miCiudad = (ciudad) -> System.out.println("Yo digo que vivo en --> " + ciudad);
		miCiudad.dondeVivo(dondeVivo);
	}
	
	//Usar lambdas con dos parámetros para especificar donde vivo
	public static void usingInterfaceLambdaWithTwoParameters() {
		String ciudad = "Marbella";
		String pueblo = "Ojén";
		
		IinterfaceWithTwoParameters interfacecv = (lmbdciudad, lmbdpueblo) -> {
			System.out.println("Pero realmente yo vivo en --> " + lmbdpueblo + " cerca de --> " + lmbdciudad);
			return lmbdpueblo + lmbdciudad;
		};
		interfacecv.ciudadYPueblo(ciudad, pueblo);
	}

}
